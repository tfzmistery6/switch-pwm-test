#include "stm32f10x.h"

// Мёртвое время считается по следующей формуле:
// (10^9/72'000'000) * DEADTIME = время в наносекундах
#define DEADTIME 50

void TIM1_init(void)
{
	//Инициализация выводов GPIO
	RCC -> APB2ENR |= RCC_APB2ENR_IOPAEN; //Включаем тактирование порта А
	RCC -> APB2ENR |= RCC_APB2ENR_IOPBEN; //Включаем тактирование порта B
	RCC -> APB2ENR |= RCC_APB2ENR_AFIOEN; //Включаем альтернативные функции ввода вывода
	
	AFIO -> MAPR |= (1 << 6); //Частичный ремап каналов TIM1
	
	//PA8 - TIM1_CH1
	//PA9 - TIM1_CH2
	//PA10 -TIM1_CH3
	//PA7 - TIM1_CH1N
	//PB0 - TIM1_CH2N
	//PB1 - TIM1_CH3N
	GPIOA -> CRL |= (1 << 28 | 1 << 29); //PA7 режим вывода 50 МГц
	GPIOA -> CRL &= ~(1 << 30); //здесь на ресете ненужная еденичка
	GPIOA -> CRL |= (1 << 31); //PA7 альтернативный режим push-pull
	
	//PA8 PA9 PA10 режим вывода 50 МГц
	GPIOA -> CRH |= (1 << 0 | 1 << 1 | 1 << 4 | 1 << 5 | 1 << 8 | 1 << 9);
	GPIOA -> CRH &= ~(1 << 2 | 1 << 6 | 1 << 10); //Здесь на ресете ненужные еденички
	GPIOA -> CRH |= (1 << 3 | 1 << 7 | 1 << 11); //PA8 PA9 PA10 альтернативный режим push-pull
	
	GPIOB -> CRL |= (1 << 0 | 1 << 1 | 1 << 4 | 1 << 5); //PB0 PB1 режим вывода 50 МГц
	GPIOB -> CRL &= ~(1 << 2 | 1 << 6); //Здесь на ресете ненужные еденички
	GPIOB -> CRL |= (1 << 3 | 1 << 7); //PB0 PB1 альтернативный режим push-pull
	
	
	//Произведём настройку таймера
	RCC -> APB2ENR |= RCC_APB2ENR_TIM1EN; //Включаем тактирование таймера
	
	TIM1 -> CR1 |= TIM_CR1_ARPE; //Включаем preload для регистра TIM1_ARR
	TIM1 -> CR1 |= TIM_CR1_CMS; //Режим 3 выравнивания по центру
	//Активируем режим PWM 2 и включаем preload для регистра TIM1_CCR
	TIM1 -> CCMR1 |= TIM_CCMR1_OC1M_2 | TIM_CCMR1_OC1M_1 | TIM_CCMR1_OC1M_0 | TIM_CCMR1_OC1PE; //канал 1
	TIM1 -> CCMR1 |= TIM_CCMR1_OC2M_2 | TIM_CCMR1_OC2M_1 | TIM_CCMR1_OC2M_0 | TIM_CCMR1_OC2PE; //канал 2
	TIM1 -> CCMR2 |= TIM_CCMR2_OC3M_2 | TIM_CCMR2_OC3M_1 | TIM_CCMR2_OC3M_0 | TIM_CCMR2_OC3PE; //канал 3
	//Включаем основной и комплиментарный выход
	TIM1 -> CCER |= TIM_CCER_CC1E | TIM_CCER_CC1NE; //канал 1
	TIM1 -> CCER |= TIM_CCER_CC2E | TIM_CCER_CC2NE; //канал 2
	TIM1 -> CCER |= TIM_CCER_CC3E | TIM_CCER_CC3NE; //канал 3
	TIM1 -> BDTR |= TIM_BDTR_MOE | DEADTIME;
	TIM1 -> PSC = 7; //Делитель частоты TIM1 8 -> частота 72/8 = 9 МГц
	TIM1 -> ARR = 150; //Autoreload (по факту разрешение скважности) //30 кГц
	TIM1 -> CCR1 = 75; //Начальное значение скважности канал 1
	TIM1 -> CCR2 = 75; //Начальное значение скважности канал 2
	TIM1 -> CCR3 = 75; //Начальное значение скважности канал 3
	TIM1 -> CR1 |= TIM_CR1_CEN;//Включить TIM1
	TIM1 -> EGR |= TIM_EGR_UG; //Сразу генерируем событие обновления
}
