#include "stm32f10x.h"

void GPIO_init(void)
{
	RCC -> APB2ENR |= RCC_APB2ENR_IOPBEN; //Включим тактирование порта B
	
	//Настроим PB8 PB9 как входы кнопок 
	GPIOB -> CRH &= ~(1 << 2 | 1 << 6); //Сбросим ненужные еденички
	GPIOB -> CRH |= (1 << 3 | 1 << 7); //включим режим входа с подтяжкой
	GPIOB -> ODR |= (GPIO_ODR_ODR8 | GPIO_ODR_ODR9); //Укажем подтяжку к плюсу
	
	RCC -> APB2ENR |= RCC_APB2ENR_IOPCEN; //включим тактирование порта C
	//PC13
	GPIOC -> CRH |= (1 << 20); //включим режим выхода open_drain
}

